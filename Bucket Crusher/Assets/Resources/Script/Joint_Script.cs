using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Joint_Script : MonoBehaviour
{
    public GameObject m_child;
    public GameObject GetChild(){
        return m_child; 
    }

    public void Rotate(float _angle){
        transform.Rotate(new Vector3(0,0,1) * _angle);
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
    }
}
