using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawn_Brick_Script : MonoBehaviour
{
    public List<GameObject> objectList= new List<GameObject>();
    // Start is called before the first frame update
    void Start()
    {
        Sprite sp = Resources.Load<Sprite>("Sprites/brick") as Sprite;
        var y = -5f + 0.68f + 0.3f;
        for(int i = 0; i <= 40 * 40 - 1; i++){
            var x = (i % 40);
            if(i % 40 == 0)
                y += 0.6915f;
            GameObject gameObject = new GameObject("Test Object " + i.ToString());
            SpriteRenderer spriteRenderer = gameObject.AddComponent<SpriteRenderer>();
            BoxCollider2D boxCollider = gameObject.AddComponent<BoxCollider2D>();
            Rigidbody2D gameObjectsRigidBody = gameObject.AddComponent<Rigidbody2D>();

            gameObject.transform.position = new Vector3(x * 0.6915f, y, 0);
            // gameObjectsRigidBody.bodyType = RigidbodyType2D.Static;
            gameObjectsRigidBody.gravityScale = 0.01f;
            spriteRenderer.sprite = sp;
            boxCollider.size = new Vector2(0.68f, 0.68f);
            objectList.Add(gameObject);
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
