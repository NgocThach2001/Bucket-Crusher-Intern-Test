using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blade_Script : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameObject root_chain;
    public GameObject head_chain;
    public float max_distance = 0;
    bool canPlayerMove = true;
    public float speed = 1.0f;
    public float acceleration = 1.0f;
    void Start()
    {
        InvokeRepeating(nameof(IncreaseSpeed), 1f, 1f);
        rb = gameObject.GetComponent<Rigidbody2D>();
        rb.gravityScale = 0;
    }

    public void IncreaseSpeed(){
        speed += acceleration;
        if(speed > 5.0f)
            speed = 5.0f;
    }

    float GetDistance(Vector3 _point1, Vector3 _point2){
        return Vector3.Distance(_point1, _point2);
    }

    void Update()
    { 
        var d1 = GetDistance(root_chain.transform.position, head_chain.transform.position);
        if(max_distance < d1)
            max_distance = d1;
        var d2 = GetDistance(root_chain.transform.position, this.transform.position);
        if(d2 > max_distance){
            this.transform.position = head_chain.transform.position;
        }
        transform.Rotate(new Vector3(0, 0, -270 * Time.deltaTime));
        bool moveLeft = Input.GetKey(KeyCode.LeftArrow);
        bool moveRight = Input.GetKey(KeyCode.RightArrow);
        bool moveUp = Input.GetKey(KeyCode.UpArrow);
        bool moveDown = Input.GetKey(KeyCode.DownArrow);
        if (canPlayerMove)
         {
            var moveX = 0;
            var moveY = 0;
            if(moveLeft) moveX = -1;
            if(moveRight) moveX = 1;
            if(moveDown) moveY = -1;
            if(moveUp) moveY = 1;
            Vector3 movement = new Vector3(moveX, moveY, 0.0f);
            transform.Translate(movement.normalized * speed * Time.deltaTime, Space.World);
         }
    }
}
