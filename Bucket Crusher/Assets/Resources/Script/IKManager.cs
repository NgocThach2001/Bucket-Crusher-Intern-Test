using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IKManager : MonoBehaviour
{
    public GameObject m_root;
    public GameObject m_end;
    public GameObject m_target;

    public float m_threshold = 0.05f;
    public float m_rate = 5.0f;
    public int m_steps = 20;

    float CalculateSlope(GameObject _joint){
        float delTheta = 0.01f;
        float distance1 = GetDistance(m_end.transform.position, m_target.transform.position);
        var scriptName = _joint.GetComponent<Joint_Script>();
        scriptName.Rotate(delTheta);
        float distance2 = GetDistance(m_end.transform.position, m_target.transform.position);
        scriptName.Rotate(-delTheta);
        return (distance2 - distance1) / delTheta;
    }

    float GetDistance(Vector3 _point1, Vector3 _point2){
        return Vector3.Distance(_point1, _point2);
    }
    
    void Start()
    {
        
    }

    void Update()
    {
        for(int i = 0; i < m_steps; i++){
            if (GetDistance(m_end.transform.position, m_target.transform.position) > m_threshold){
                GameObject current = m_root;
                while(current != null){
                    float slope = CalculateSlope(current);
                    var scriptName = current.GetComponent<Joint_Script>();
                    scriptName.Rotate(-slope * m_rate);
                    current = scriptName.GetChild();
                }
            }
        }
        
    }
}
